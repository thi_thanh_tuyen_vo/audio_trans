#importation des divers modules
import pygame
import serial
from tkinter import *
from playsound import playsound
from pygame.locals import *


#initialisation de la communication avec la carte arduino, du module pygame et des variables
ser = serial.Serial('COM3', baudrate=9600, timeout=1)
pygame.init()
potato = 1
filter_selected=1



#déclaration des fonctions nécessaires aux boutons de choix
def setlow():
    global filter_selected,tele
    filter_selected=1 #On modifie le filtre choisi
    tele.destroy() #on ferme la fenêtre de choix
    return

def setnorm():
    global filter_selected,tele
    filter_selected = 0
    tele.destroy()
    return

def sethigh(): 
    global filter_selected, tele
    filter_selected = 2
    tele.destroy()
    return

def exitpg():
    global potato,tele
    potato = 0 #sortie de la boucle infinie
    tele.destroy()
    return


#Boucle infinie principale du programme
while potato :
    """Création de la fenêtre de choix"""
    tele = Tk()
    tele.resizable(width=False, height=False)
    tele.title("Choix du filtre")#changement du titre de la fenêtre
    tele.call('wm', 'iconphoto', tele._w, PhotoImage(file='icon.gif'))#changement de l'icone de la fenêtre (on affiche l'image "icon.gif")
    tele.geometry("350x150")
    canv = Canvas(tele, bg="#cccccc", width="348", height="148")
    canv.place(x=1, y=1)
    #création + placement des boutons
    lowbutton = Button(tele, text="Surdité de perception", command=setlow)
    normalbutton = Button(tele, text="Normal", command=setnorm)
    highbutton = Button(tele, text="Surdité de transmission", command=sethigh)
    exitbutton = Button(tele, text="Quitter", command=exitpg)
    lowbutton.place(x=10, y=70)
    normalbutton.place(x=150,y=70)
    highbutton.place(x=215, y=70)
    exitbutton.place(x=300,y=120)
    tele.mainloop()#Boucle infinie de l'écran de choix
    if potato == 0 :#si on a cliqué sur le bouton quitter : Sortie de la boucle infinie
        break
    """Création de la partie jouant les sons"""
    pygame.display.init()#initialisation du module d'affichage de pygame, nécessaire à la récupération des inputs claviers
    display_width = 800
    display_height = 600
    screen = pygame.display.set_mode((display_width, display_height))
    pygame.display.set_caption("Transformateur de son !")#changement du titre de la fenêtre
    pygame.display.set_icon(pygame.image.load("icon.jpg"))#changement du logo de la fenêtre
    continuer = 1 #boucle infinie de la partie des sons
    while continuer:  # boucle du jeu
        arduinoData = ser.readline().decode('ascii').rstrip()#on récupère les données envoyées par la carte arduino
        for event in pygame.event.get():
            arduinoData = ser.readline().decode('ascii').rstrip()
            if event.type == QUIT:  # Quand on ferme le fenetre on sort de la boucle
                continuer = 0
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:  # Quand on appuie sur échap, on sort de la boucle et on ferme la fenêtre
                    continuer = 0

                if event.key == K_LEFT:  # Si la touche flèche du bas est appuyé, on lance la note correspondante
                    if filter_selected==0 :#si le filtre sélectionné est celui d'une audition normale alors on joue le son normal
                        playsound("sounds_wav/0.wav",False)
                    elif filter_selected==1 :
                        playsound("lsounds_wav/l0.wav",False)
                    elif filter_selected==2 :
                        playsound("hsounds_wav/h0.wav",False)

                if event.key == K_UP:
                    if filter_selected==0 :
                        playsound("sounds_wav/1.wav",False)
                    elif filter_selected==1 :
                        playsound("lsounds_wav/l1.wav",False)
                    elif filter_selected == 2:
                        playsound("hsounds_wav/h1.wav",False)

                if event.key == K_RIGHT:
                    if filter_selected==0 :
                        playsound("sounds_wav/2.wav",False)
                    elif filter_selected==1 :
                        playsound("lsounds_wav/l2.wav",False)
                    elif filter_selected == 2:
                        playsound("hsounds_wav/h2.wav",False)

                if event.key == K_DOWN:
                    if filter_selected==0:
                        playsound("sounds_wav/3.wav",False)
                    elif filter_selected==1 :
                        playsound("lsounds_wav/l3.wav",False)
                    elif filter_selected == 2:
                        playsound("hsounds_wav/h3.wav",False)

                if event.key == K_SPACE:
                    if filter_selected==0 :
                        playsound("sounds_wav/4.wav",False)
                    elif filter_selected==1 :
                        playsound("lsounds_wav/l4.wav",False)
                    elif filter_selected == 2:
                        playsound("hsounds_wav/h4.wav",False)

                if arduinoData !="0": #même chose mais avec les données envoyées par la arduino
                    if arduinoData=="H":
                        if filter_selected==0 :
                            playsound("sounds_wav/7.wav",False)
                        elif filter_selected==1 :
                            playsound("lsounds_wav/l7.wav",False)
                        elif filter_selected == 2:
                            playsound("hsounds_wav/h7.wav",False)

                    if arduinoData=="A":
                        if filter_selected==0:
                            playsound("sounds_wav/5.wav",False)
                        elif filter_selected==1 :
                            playsound("lsounds_wav/l5.wav",False)
                        elif filter_selected==2 :
                            playsound("hsounds_wav/h5.wav",False)

                    if arduinoData=="B":
                        if filter_selected==0:
                            playsound("sounds_wav/6.wav",False)
                        elif filter_selected==1 :
                            playsound("lsounds_wav/l6.wav",False)
                        elif filter_selected==2 :
                            playsound("hsounds_wav/h6.wav",False)

        if arduinoData !="0":
            if arduinoData=="H":
                if filter_selected==0 :
                    playsound("sounds_wav/7.wav",False)
                elif filter_selected==1 :
                    playsound("lsounds_wav/l7.wav",False)
                elif filter_selected == 2:
                    playsound("hsounds_wav/h7.wav",False)

            if arduinoData=="A":
                if filter_selected==0:
                    playsound("sounds_wav/5.wav",False)
                elif filter_selected==1 :
                    playsound("lsounds_wav/l5.wav",False)
                elif filter_selected==2 :
                    playsound("hsounds_wav/h5.wav",False)

            if arduinoData=="B":
                if filter_selected==0:
                    playsound("sounds_wav/6.wav",False)
                elif filter_selected==1 :
                    playsound("lsounds_wav/l6.wav",False)
                elif filter_selected==2 :
                    playsound("hsounds_wav/h6.wav",False)
    pygame.display.quit()#on détruit la fenêtre pygame
pygame.quit()#on détruit le module pygame
 