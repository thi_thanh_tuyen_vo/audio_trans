from scipy.io import wavfile
from scipy import signal
import numpy as np
for loop in range(8):
    fnameh = str(loop) + '.wav'
    outnameh = 'h' + str(loop) + '.wav'
    sr, x = wavfile.read(fnameh)    # 16-bit mono 44.1 khz

    b = signal.firwin(101, cutoff=44100, fs=sr, pass_zero=False)
    print(sr)
    x = signal.lfilter(b, [1.0], x)
    wavfile.write(outnameh, sr, x.astype(np.int16))
    input()
input()